import React, {Component} from 'react';
import {View, Text, StyleSheet, TextInput, Alert, Button} from 'react-native';

import Message from './app/components/message/Message';
import Body from './app/components/body/Body';
import OurFlatList from './app/components/ourFlatList/ourFlatList';
import ConexionFetch from './app/components/conexionFetch/ConexionFetch';

import {NavigationContainer, StackActions} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

const provincias = [
  {
    id: 1,
    name: 'Arequipa',
  },
  {
    id: 2,
    name: 'Puno',
  },
  {
    id: 3,
    name: 'Cuzco',
  },
];

function HomeScreen({navigation}) {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Home Screen</Text>
      <Button
        title="Go to Details"
        onPress={() => navigation.navigate('Details')}
      />
      <Button
        title="Replace Details"
        onPress={() => navigation.replace('Details')}
      />
    </View>
  );
}

function DetailsScreen({navigation}) {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Details Screen</Text>
      <Button
        title="Go to Details... again"
        onPress={() => navigation.navigate('Nuevo')}
      />
    </View>
  );
}
function NuevoScreen({navigation}) {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Nuevo Screen</Text>
      <Button
        title="Go to Details"
        onPress={() =>
          navigation.reset({
            index: 0,
            routes: [
              {
                name: 'Details',
              },
            ],
          })
        }
      />
    </View>
  );
}

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      textValue: '',
      count: 0,
    };
  }

  changeTextInput = textoInsertado => {
    console.log(textoInsertado);

    this.setState({textValue: textoInsertado});
  };
  onPress = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };

  showAlert = () => {
    Alert.alert(
      'TItulos',
      'Mensaje',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Canel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OKs',
          onPress: () => console.log('OK Pressed'),
        },
      ],
      {cancelable: false},
    );
  };

  /*   render() {
    return <OurFlatList showAlert={this.showAlert} />;
  } */

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" component={HomeScreen} />

          <Stack.Screen name="Details" component={DetailsScreen} />
          <Stack.Screen name="Nuevo" component={NuevoScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }

  /*
  render() {
    return (
      <View style={styles.container}>
        <OurFlatList />
      </View>
    );
  }*/

  /*
  render() {
    return (
      <View style={styles.container}>
        <Message />
        <View style={styles.text}>
          <Text> Ingrese su edad</Text>
        </View>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          onChangeText={texto => this.changeTextInput(texto)}
          value={this.state.textValue}
        />

        <Body textBody={'Texto en Body'} onBodyPress={this.onPress} />

        <View style={styles.countContainer}>
          <Text style={styles.countText}>{this.state.count}</Text>
        </View>
        {provincias.map(item => (
          <View>
            <Text>{item.name}</Text>
          </View>
        ))}
      </View>
    );
  }*/
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  text: {
    alignItems: 'center',
    padding: 10,
  },
  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },
});

export default App;
