import React from 'react';
import {Text, View} from 'react-native';

const Message = () => (
  <View>
    <Text>Este es mi mensaje desde el componente Message</Text>
  </View>
);

export default Message;
